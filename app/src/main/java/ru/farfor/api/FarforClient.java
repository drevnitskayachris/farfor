package ru.farfor.api;

import retrofit.http.GET;
import ru.farfor.model.YmlCatalog;

public interface FarforClient {

    @GET("/getyml/?key=ukAXxeJYZN")
    YmlCatalog getShopContent();

}
