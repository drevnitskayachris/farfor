package ru.farfor.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "shop", strict = false)
public class Shop {

    @ElementList(name = "offers", required = false)
    private List<Offer> offers;

    public List<Offer> getOffers() {
        return offers;
    }

}
