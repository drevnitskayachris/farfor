package ru.farfor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.model.Category;
import ru.farfor.ui.adapter.CategoriesAdapter;
import ru.farfor.ui.custom.RecyclerItemClickListener;
import ru.farfor.ui.utils.FragmentUtils;


public class CategoryFragment extends Fragment {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;
    private CategoriesAdapter rvAdapter;
    private List<Category> categories;

    public static CategoryFragment newInstance() {

        Bundle args = new Bundle();
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        categories = readCategories(R.raw.catogories);
        rvAdapter = new CategoriesAdapter(getActivity(), categories);
        recyclerView.setAdapter(rvAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onCategoryClicked(position);
                    }
                }));

        getActivity().setTitle(getString(R.string.category_screen_title));
    }

    private void onCategoryClicked(int position) {
        int categoryId = categories.get(position).getId();
        OffersListFragment offersFragment = OffersListFragment.newInstance(categoryId);
        FragmentUtils.replaceFragment(getActivity(), R.id.content_frame, offersFragment, true);
    }

    private List<Category> readCategories(int file) {
        InputStream is = getResources().openRawResource(file);
        String str = "";
        try {
            str = IOUtils.toString(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        IOUtils.closeQuietly(is);

        Type listType = new TypeToken<List<Category>>() {}.getType();
        List<Category> categories = new Gson().fromJson(str, listType);

        return categories;

    }
}
