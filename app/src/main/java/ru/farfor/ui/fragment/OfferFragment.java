package ru.farfor.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.model.Offer;
import ru.farfor.ui.adapter.OffersAdapter;

public class OfferFragment extends Fragment {

    static private final String SELECTED_OFFER = "offer";

    @Bind(R.id.offer_icon)
    ImageView offerIcon;
    @Bind(R.id.offer_title)
    TextView offerTitle;
    @Bind(R.id.offer_weight)
    TextView offerWeight;
    @Bind(R.id.offer_price)
    TextView offerPrice;
    @Bind(R.id.offer_description)
    TextView offerDescription;

    public static OfferFragment newInstance(Offer offer) {

        Bundle args = new Bundle();
        args.putSerializable(SELECTED_OFFER, offer);
        OfferFragment fragment = new OfferFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_offer, container, false);
        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(getString(R.string.offer_screen_title));

        Offer offer = (Offer) getArguments().getSerializable(SELECTED_OFFER);
        if(null != offer.getPictureUrl()) {
            OffersAdapter.picassoOfferIcon(getContext(), offer.getPictureUrl(), offerIcon);
        }
        offerTitle.setText(offer.getName());
        if (offer.getParam() != null) {
            String weight = offer.getParam().get("Вес");
            if (weight != null) {
                offerWeight.setText(getContext().getString(R.string.weight, weight));
            } else {
                offerWeight.setText(getContext().getString(R.string.weight, "-"));
            }
        } else {
            offerWeight.setText(getContext().getString(R.string.weight, "-"));
        }
        offerPrice.setText(getContext().getString(R.string.price, offer.getPrice()));
        if (offer.getDescription() != null) {
            offerDescription.setText(offer.getDescription());
        }

    }
}
