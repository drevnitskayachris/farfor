package ru.farfor.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.model.Offer;

public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.OfferViewHolder>{

    private Context ctx;
    private List<Offer> offers;

    public static class OfferViewHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.offer_icon)
        ImageView offerIcon;
        @Bind(R.id.offer_title)
        TextView offerTitle;
        @Bind(R.id.offer_weight)
        TextView offerWeight;
        @Bind(R.id.offer_price)
        TextView offerPrice;

        public OfferViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public OffersAdapter(Context ctx, List<Offer> offers) {
        this.ctx = ctx;
        this.offers = offers;
    }

    @Override
    public OfferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_list_item, parent, false);

        return new OfferViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OfferViewHolder holder, int position) {

        Offer offer = offers.get(position);
        String pictureUrl = offer.getPictureUrl();
        if (pictureUrl != null) {
            picassoOfferIcon(ctx, pictureUrl, holder.offerIcon);
        } else {
            holder.offerIcon.setImageDrawable(ContextCompat.getDrawable(ctx, R.drawable.default_img));
        }
        holder.offerTitle.setText(offer.getName());
        if (offer.getParam() != null) {
            String weight = offer.getParam().get("Вес");
            if (weight != null) {
                holder.offerWeight.setText(ctx.getString(R.string.weight, weight));
            } else {
                holder.offerWeight.setText(ctx.getString(R.string.weight, "-"));
            }
        } else {
            holder.offerWeight.setText(ctx.getString(R.string.weight, "-"));
        }
        holder.offerPrice.setText(ctx.getString(R.string.price, offer.getPrice()));
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    static public void picassoOfferIcon(Context ctx, final String ref, ImageView imgView){
        Picasso picassoInstance = Picasso.with(ctx);
        picassoInstance.load(ref)
                .fit()
                .centerCrop()
                .placeholder(ContextCompat.getDrawable(ctx, R.drawable.default_img))
                .into(imgView);
    }

}
