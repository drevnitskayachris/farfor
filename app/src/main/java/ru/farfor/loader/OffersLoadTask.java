package ru.farfor.loader;

import android.content.Context;

import ru.farfor.api.request.FarforApiRequest;
import ru.farfor.model.Shop;
import ru.farfor.model.YmlCatalog;

public class OffersLoadTask extends AsyncTaskLoaderEx<Shop> {

    public OffersLoadTask(Context ctx) {
        super(ctx);
    }

    @Override
    public Shop loadInBackground() {

        try {
            YmlCatalog catalog = FarforApiRequest.getAPI().getShopContent();
            return catalog.getShop();
        } catch (Exception ex) {
            return null;
        }

    }

}
