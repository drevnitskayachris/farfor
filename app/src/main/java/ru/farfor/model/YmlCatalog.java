package ru.farfor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "yml_catalog", strict = false)
public class YmlCatalog {
    @Element(name = "shop")
    private Shop shop;

    public Shop getShop() {
        return shop;
    }
}
