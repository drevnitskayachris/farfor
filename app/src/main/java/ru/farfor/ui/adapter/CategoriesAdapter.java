package ru.farfor.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.model.Category;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CategoryViewHolder> {

    private List<Category> categories;
    private Context ctx;


    public static class CategoryViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.category_icon)
        ImageView categoryIcon;
        @Bind(R.id.category_title)
        TextView categoryTitle;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public CategoriesAdapter(Context ctx, List<Category> categories) {
        this.ctx = ctx;
        this.categories = categories;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item, parent, false);

        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        Category category = categories.get(position);

        int resourceId = ctx.getResources().getIdentifier("drawable/_" + category.getId(),
                "drawable", ctx.getPackageName());
        holder.categoryIcon.setImageDrawable(ContextCompat.getDrawable(ctx, resourceId));
        holder.categoryTitle.setText(category.getCategoryTitle());

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
