package ru.farfor.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.loader.OffersLoadTask;
import ru.farfor.model.Offer;
import ru.farfor.model.Shop;
import ru.farfor.ui.adapter.OffersAdapter;
import ru.farfor.ui.custom.RecyclerItemClickListener;
import ru.farfor.ui.utils.FragmentUtils;

public class OffersListFragment extends Fragment implements LoaderManager.LoaderCallbacks<Shop>{

    static private final String CATEGORY_ID = "categoryId";
    static private final int TASK_ID = 1;
    private List<Offer> offers = new ArrayList<>();
    private OffersAdapter adapter;
    private  int categoryId;

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    public static OffersListFragment newInstance(int categoryId) {

        Bundle args = new Bundle();
        args.putInt(CATEGORY_ID, categoryId);
        OffersListFragment fragment = new OffersListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category, container, false);

        ButterKnife.bind(this, rootView);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getActivity().setTitle(getString(R.string.offers_list_screen_title));

        categoryId = getArguments().getInt(CATEGORY_ID);

        getOffersList();

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(llm);
        adapter = new OffersAdapter(getActivity(), offers);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(),
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        onOfferClicked(position);
                    }
                }));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getLoaderManager().getLoader(TASK_ID).reset();

    }

    private void onOfferClicked(int position) {
        OfferFragment offerFragment = OfferFragment.newInstance(offers.get(position));
        FragmentUtils.replaceFragment(getActivity(), R.id.content_frame, offerFragment, true);
    }

    private void getOffersList() {
        getLoaderManager().initLoader(TASK_ID, null, this);

    }

    @Override
    public Loader<Shop> onCreateLoader(int id, Bundle args) {
        if (id == TASK_ID) {
            return new OffersLoadTask(getContext());
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Shop> loader, final Shop data) {
        if(data != null) {
            offers.clear();
            List<Offer> categoryOffers = new ArrayList<>();
            for(Offer offer: data.getOffers()) {
                if (offer.getCategoryId() == categoryId) {
                    categoryOffers.add(offer);
                }
            }
            offers.addAll(categoryOffers);
            adapter.notifyDataSetChanged();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.error_title)
                    .setMessage(R.string.error_msg)
                    .setCancelable(false)
                    .setNeutralButton(R.string.neutral_btn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    @Override
    public void onLoaderReset(Loader<Shop> loader) {

    }
}
