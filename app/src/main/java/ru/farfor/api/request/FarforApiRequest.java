package ru.farfor.api.request;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.SimpleXMLConverter;
import ru.farfor.api.FarforClient;

public class FarforApiRequest {

    public static final String URL = "http://ufa.farfor.ru";

    static FarforClient farforApi;

    private static void createAPI(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36");
                    }
                })
                .setConverter(new SimpleXMLConverter())
                .build();
        farforApi = restAdapter.create(FarforClient.class);
    }

    public static FarforClient getAPI(){
        if (farforApi == null){
            createAPI();
        }

        return farforApi;
    }
}
