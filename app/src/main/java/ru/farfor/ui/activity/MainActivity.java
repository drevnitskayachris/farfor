package ru.farfor.ui.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import ru.farfor.R;
import ru.farfor.ui.fragment.CategoryFragment;
import ru.farfor.ui.fragment.GoogleMapViewFragment;
import ru.farfor.ui.utils.FragmentUtils;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.drawer_layout) DrawerLayout drawer;
    @Bind(R.id.nav_view) NavigationView navigationView;
    private  ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        CategoryFragment categoryFragment = CategoryFragment.newInstance();
        FragmentUtils.replaceFragment(this, R.id.content_frame, categoryFragment, false);

        getSupportFragmentManager().addOnBackStackChangedListener(backStackListener);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSupportNavigateUp();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.nav_catalogue:
                CategoryFragment categoryFragment = CategoryFragment.newInstance();
                FragmentUtils.replaceFragment(this, R.id.content_frame, categoryFragment, false);
                break;
            case R.id.nav_direction:
                GoogleMapViewFragment googleMapFragment = GoogleMapViewFragment.newInstance();
                FragmentUtils.replaceFragment(this, R.id.content_frame, googleMapFragment, false);
                break;
            default:
                break;

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        public void onBackStackChanged() {
            setNavIcon();
        }
    };

    protected void setNavIcon() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        toggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.isDrawerIndicatorEnabled() && toggle.onOptionsItemSelected(item)) {
            return true;
        } else if (item.getItemId() == android.R.id.home &&
                getSupportFragmentManager().popBackStackImmediate()) {
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }

    }

    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
