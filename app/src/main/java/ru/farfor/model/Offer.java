package ru.farfor.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Root(name = "offer", strict = false)
public class Offer implements Serializable{
    @Element(name = "categoryId")
    int categoryId;

    @Element(name = "name")
    String name;

    @Element(name = "picture", required = false)
    String pictureUrl;

    @Element(name = "price")
    String price;

    @Element(name = "description", required = false)
    String description;

    @ElementMap(entry="param", key="name", attribute=true, inline=true, required = false)
    HashMap<String, String> param;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }


    public String getDescription() {
        return description;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public Map<String, String> getParam() {
        return param;
    }

    public int getCategoryId() {
        return categoryId;
    }
}
